# 2023-03-04

Narasi:
- Pendukung calon tertentu memiliki polarisasi yang lebih tinggi

Task:
- Sub-group analysis pada kelompok Islam dengan latent class analysis
- IRT analysis untuk item 33
- Cari segregasi natural dari seluruh variabel
- Cari segregasi

